/*
   RGBU class
   the class contains information of red, green, blue and ultraviolet channels for LED strips
   @author: Samuli "Handicap" Kinnunen
   @date 22.12.2017
   @version 1.3
*/

/*
   Rgbu class definition
   r, g, b, u - self explanatory, u is for UV
*/

class Rgbu {
  private:

  public:
    byte r, g, b, u;
    Rgbu();
    Rgbu(byte, byte, byte, byte);
};

// generic constructors
Rgbu::Rgbu() {
  r = 0;
  b = 0;
  g = 0;
  u = 0;
}

Rgbu::Rgbu (byte rin, byte gin, byte bin, byte uin) {
  r = rin;
  g = gin;
  b = bin;
  u = uin;
}

/*
   THIS IS BAD PRACTICE, I'm just lazy, drunk and haven't been programming c++ for a while
   FIX: use a .cpp-file
*/

float static LerpFloat(float first, float last, float phase) {
  return first + phase * (last - first);
}


/*
 * Linear interpolation between two instances of Rgbu
 * Parameters:
 * first: the value to fade from
 * second: the value to fade to
 * phase: the "phase" of the interpolation, ranges from 0.f to 1.f
 * 
 * returns a new Rgbu object according to the specified phase
 */
Rgbu static LerpColors(Rgbu first, Rgbu last, float phase) {
  Rgbu ret;
  ret.r = LerpFloat(first.r, last.r, phase);
  ret.g = LerpFloat(first.g, last.g, phase);
  ret.b = LerpFloat(first.b, last.b, phase);
  ret.u = LerpFloat(first.u, last.u, phase);

  return ret;

}

Rgbu static RandomColor(float intensity){
  
  srand (static_cast <unsigned> ((0)));

  // Todo: intensity normalization
  Rgbu ret;

  float rweight = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  float gweight = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  float bweight = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

  
  int maxbright = rand() % 255 * 3;
  
  ret.r = rand() % 255;
  maxbright -= ret.r;
  ret.g = rand() % 255;
  maxbright -= ret.g;
  ret.b = rand() % 255;
  maxbright -= ret.g;
  ret.u = rand() % 255;

  return ret;
}

void PrintColors(Rgbu printTarget) {
  Serial.print(printTarget.r);
  Serial.print(printTarget.g);
  Serial.println(printTarget.b);

}
