/*

  Fades an LED (strip) on and off depending if there is any motion detected by the PIR

  @author: Samuli "Handicap" Kinnunen
  @date 06.04.2017
  @version 1.7

  Used and modified Kristian Gohlke's code for more reliable PIR behavior, header pasted below.

  Handicap's notes on building the LED-strip mosfet-based LED-strip controller.

  On a breadboard: 12v power supply:


  General assembly guide (v1.3):

  -----------------------------------------------

  Connect an LED strip as follows:
  (MOSFET text facing you: G S D)

  G to an arduino PWM pin
  Drain to LED GND
  Source to GND
  LED positive, or color, to Vin


  PIR-SENSOR:
  connect PIR VCC to arduino 5v output
  connect PIR OUT to arduino 3 digital pin
  connect PIR GND to PSU/arduino GND

  -----------------------------------------------


  Specific for UV-led (version 1.0)

  ARDUINO:
  connect the arduino GND to psu GND
  connect the positive from PSU to arduino (vin)

  LED:
  connect the MOSFET as follows:
      (note the MOSFET scheme (G, S, D when the text is facing you)
    GATE to arduino 9 digital pin
    DRAIN to LED strip GND
    SOURCE to PSU/arduino GND
  connect the positive from the LED to PSU positive

  PIR-SENSOR:
  connect PIR VCC to arduino 5v output
  connect PIR OUT to arduino 3 digital pin
  connect PIR GND to PSU/arduino GND

  Note that in order for the MOSFET to operate, the baseboard and power supply must be grounded together

  For extra functionality, add a 2-way on/off switch to the power source

  TODO: maybe a potentiometer for easy manipulation of fading after assembly

  Happy hunting!
*/

/*
  //////////////////////////////////////////////////
  //making sense of the Parallax PIR sensor's output
  //////////////////////////////////////////////////

  Switches a LED according to the state of the sensors output pin.
  Determines the beginning and end of continuous motion sequences.

  @author: Kristian Gohlke / krigoo (_) gmail (_) com / http://krx.at
  @date:   3. September 2006

  kr1 (cleft) 2006
  released under a creative commons "Attribution-NonCommercial-ShareAlike 2.0" license
  http://creativecommons.org/licenses/by-nc-sa/2.0/de/


  The Parallax PIR Sensor is an easy to use digital infrared motion sensor module.
  (http://www.parallax.com/detail.asp?product_id=555-28027)

  The sensor's output pin goes to HIGH if motion is present.
  However, even if motion is present it goes to LOW from time to time,
  which might give the impression no motion is present.
  This program deals with this issue by ignoring LOW-phases shorter than a given time,
  assuming continuous motion is present during these phases.

*/

/////////////////////////////
//VARS

// include the RGB and UV data structure
// helps keeping track of things, object oriented programming bitches
#include "RGBU.h"
//#include <vector.h> //thought about using c++ vectors here, but too much of a hassle
//#include "Potentiometer.h"
//using namespace Rgbu;

// digital pinning
#define RED_LED 3
#define GREEN_LED 11
#define BLUE_LED 5
#define UV_LED 9
#define PIR_PIN 12
#define TEST_LED 13
#define SPEED_PIN A0
#define PROG_PIN A1

// for serial monitor and startup function tests
#define DEBUG false
#define STARTCHECK true


/* The next parameters control the PIR:
   Calibration time is set to let the PIR internals time to calibrate
   correct values are from 10 to 60 (seconds)

   lowIn stores the time when motion is detected
   after that, the system waits until the motionPause duration
   is elapsed (in millis.), then the system reacts accordingly

   waitingForMotion is set when the system is ready to activate again,
   when the system is in this mode, the system reacts to the pir,
   otherwise the system skips any registered (or unregistered) input

   motionTimerStarted simply checks if the signal has been given to
   start the timer, it is set to false when the timer runs out
   and to true when the timer is set for that input measurement
*/
const unsigned int calibrationTime = 12;
long unsigned int motionStopTime;
const long unsigned int motionPause = 500;
boolean waitingForMotion = true;
boolean motionTimerStarted = false;

boolean effectOn = false;

//TODO: blinkwithoutdelay-style functioning better


long unsigned int currentMillis = 0;


int rBright = 0;   // LED brightnesses
int gBright = 0;
int bBright = 0;
int uBright = 0;

int fadeAmount = 5;    // how many points to fade the LED by
int fadeDir = 0;       // fading direction/multiplier
int maxBrightness = 255;
int minBrightness = 0;


// some common colors
const Rgbu black (  0,   0,   0,   0);
const Rgbu white (255, 255, 255,   0);

const Rgbu red   (255,   0,   0,   0);
const Rgbu green (  0, 255,   0,   0);
const Rgbu blue  (  0,   0, 255,   0);

const Rgbu yellow(255, 255,   0,   0);
const Rgbu teal  (  0, 255, 255,   0);
const Rgbu violet(255,   0, 255,   0);

const Rgbu orange(255, 128,   0,   0);
const Rgbu pink  (255,   0, 128,   0);

const Rgbu uv    (  0,   0,   0, 255);

const Rgbu allColors(255, 255, 255, 255);

Rgbu currentColor(  0,   0,   0,   0);

int potentiometerValue = 0;
int programValue = 0;



////////////////////////////
//LOOP
void loop() {   
  setAndReadPotentiometer(); 
  //logicalLoop();
  //setColor(uv);
  
  //setAndReadPotentiometer();
  //setAndReadProgram();
  //Serial.println(potentiometerValue);
  //Serial.println(programValue);
  //banjoSwitcher();
  
  //colorWhirl(0.1, 50);
  //setupTest();
  rgbPotentiometerTest();
  delay(1);
}

bool logicalLoop() { // fade interpolation step and delay
  float fadeStep = 0.01;
  int fadeDelay = 1 + potentiometerValue/25;
  currentMillis = millis();

  Rgbu colors[] = {
    red, green, blue
  };
  
  if (checkPir() == true) {

    Serial.println("PIRLOOP");
    if (parametrizedColorWhirl(fadeStep, fadeDelay, colors, 3, fadeDelay*100, true) == true){
      }
      else{
        crossFade(currentColor, black, fadeStep, fadeDelay*2);
        return false;
    }
  }
  return true;
}


bool rgbPotentiometerTest(){
  if (potentiometerValue < 333){
    setColor(red);
  }
  else if (potentiometerValue < 666){
    setColor(green);
  } else
    setColor(blue);
}

void crossFadePotentiometer(Rgbu first, Rgbu second, float phaseStep, int delayDivider) {
  float phase = 0.f;
  while (phase < 1.f) {
    Rgbu newcolor = LerpColors(first, second, phase);
    currentColor = newcolor;
    setColor(currentColor);
    phase += phaseStep;
    setAndReadPotentiometer();
    delay(potentiometerValue / delayDivider);    
  }
}

bool setAndReadPotentiometer(){
  potentiometerValue = analogRead(SPEED_PIN);
}

bool setAndReadProgram(){  
  programValue = analogRead(PROG_PIN);
}




/////////////////////////////
//SETUP
void setup() {
  Serial.begin(19200);
  pinMode(PIR_PIN, INPUT);
  pinMode(UV_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(BLUE_LED, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  randomSeed(analogRead(5));

  setupTest();

  // PIR calibration
  pirCalibration(calibrationTime);

}

bool setupTest(){
  

  //TODO: ifdef
  // system check
  setColor(red);
  delay(250);
  setColor(green);
  delay(250);
  setColor(blue);
  delay(250);
  setColor(uv);
  delay(250);
  //
  //// here starts drunken code
  //float phase = 0;
  //float dir = 1;
  //bool up = true;
  //while(true){
  //
  //  Rgbu newcolor = LerpColors(red, blue, phase);
  //
  //  setColor(newcolor);
  //  phase += 0.05*dir;
  //  delay(1);
  //  Serial.println(phase);
  //  //PrintColors(newcolor);
  //  if (phase > 1.f) dir = -1;
  //  if (phase < 0.f) dir = 1;
  //}
  //// end drunken kode
  //colorWhirl(50);
  //  float fadespeed = 50;
  //  while (true) {
  //    crossFade(red, yellow, fadespeed);
  //    crossFade(yellow, green, fadespeed);
  //    crossFade(green, teal, fadespeed);
  //    crossFade(teal, blue, fadespeed);
  //    crossFade(blue, violet, fadespeed);
  //    crossFade(violet, red, fadespeed);
  //  }

  
  setColor(allColors);
  delay(500);
  Serial.println("Testing all colors");
  for (int i = 0; i < 6; i ++){
    digitalWrite(TEST_LED, HIGH);
    setColor(allColors);
    delay(100);
    setColor(black);
    delay(100);
    digitalWrite(TEST_LED, LOW);
  }
  Serial.println("Color test ended");
  
  Serial.println("SETUP END");
}

void crossFade(Rgbu first, Rgbu second, float phaseStep, int delayStep) {
  float phase = 0.f;
  while (phase < 1.f) {
    Rgbu newcolor = LerpColors(first, second, phase);
    currentColor = newcolor;
    setColor(currentColor);
    phase += phaseStep;
    delay(delayStep);    
  }
}

bool uvTest(float fadespeed, int del){
  crossFade(black, uv, fadespeed, del);  
  crossFade(uv, black, fadespeed, del);  
}


// just goes back and forth between colors and returns
bool setColorBackAndForth(Rgbu startColor, Rgbu endColor, float fadeStep, int fadeDelay){  
  crossFade(startColor, endColor, fadeStep, fadeDelay);
  crossFade(endColor, startColor, fadeStep, fadeDelay);
  return true;
}

bool parametrizedColorWhirl(float fadespeed, int del, Rgbu colors[], int colorArraySize, int timeToStayOnTarget, bool startFromRandom) {
  
  setAndReadPotentiometer(); // doesn't really belong here, but useful for testing

  int i = 0;
  if (startFromRandom){
    i = random(colorArraySize);    
  }
  
  while (checkPir()){
    crossFade(currentColor, colors[i], fadespeed, del);
    delay(timeToStayOnTarget);
    Serial.print(i);
    Serial.print(" ... ");
    PrintColors(currentColor);
    i++;
    if (i >= colorArraySize) i = 0;
  }
  return false;
}

/* This method sets the color of the LEDs according to the parameters
   Parameters: red, green, blue, uv
*/
void setColor(Rgbu in)
{
  analogWrite(RED_LED, in.r);
  analogWrite(GREEN_LED, in.g);
  analogWrite(BLUE_LED, in.b);
  analogWrite(UV_LED, in.u);
//  Serial.print("setcolor: ");
//  Serial.print(in.r);
//  Serial.print(in.g);
//  Serial.print(in.b);
//  Serial.println(in.u);
}

/* This method waits for the PIR-sensor to calibrate according to the specified time

*/
void pirCalibration(unsigned int calTime) {

  //give the sensor some time to calibrate
  Serial.print("calibrating sensor with calibration time ");
  Serial.println(calTime);
  for (int i = 0; i < calTime; i++) {
    Serial.print(i);
    Serial.print(" versus ");
    Serial.println(calTime);
    //Serial.print(".");
    delay(calTime);
  }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE, PIR INITIALIZED");

}


/* This method checks if motion has happened lately
    If there is no motion, and the system is waiting for it, return false
    If there is no motion, but the system is in pause, returns false
    If there is motion, returns true
*/
bool checkPir() {
  // Motion happens
  if (digitalRead(PIR_PIN) == HIGH) {
    if (waitingForMotion) {
      //makes sure we wait for a transition to LOW before any further output is made:
      waitingForMotion = false;
      Serial.println("---");
      Serial.print("motion detected at ");
      Serial.print(millis() / 1000);
      Serial.println(" sec");
      //delay(50);
    }
    motionTimerStarted = true;
    return true;
  }
  // no motion detected
  else {
    if (motionTimerStarted) {
      motionStopTime = millis();          //save the time of the transition from high to LOW
      motionTimerStarted = false;         //make sure this is only done at the start of a LOW phase
    }
    //if the sensor is low for more than the given pause,
    //we assume that no more motion is going to happen
    if (!waitingForMotion
        &&
        millis() - motionStopTime > motionPause) {
      //makes sure this block of code is only executed again after
      //a new motion sequence has been detected
      waitingForMotion = true;
      Serial.print("motion ended at ");      //output
      Serial.print((millis() - motionPause) / 1000);
      Serial.println(" sec");
      //delay(50);

      return false;
    }
  }

}

/////////////////////////////////////////////////
/////////////////// SPECIAL PROGRAMS ////////////
/////////////////////////////////////////////////

// for Miikkus northern light set to lapland, go figure out when this guy goes "bzzrtthhhggl-bwoaaumm-bazhshdddh" trying to explain what the wants
bool northernLights(){
  float fadeStep = 0.01;
  int fadeDelay = 40;
  int currentDelay = 0;
  Rgbu brightest = uv;
  
  const Rgbu fourthStep (  0,   0,   0,   50);
  const Rgbu halfStep (  0,   0,   0,   64);
  const Rgbu fifthStep(  0,   0,   0,   180);
  const Rgbu endStep (  0,   0,   0,   220);

  //first phase:
  
  currentDelay = fadeDelay * 0.15f;
  crossFade(currentColor, fourthStep, fadeStep, currentDelay);  //TODO: see the copypaste?
  currentDelay = fadeDelay * 0.35f;
  crossFade(currentColor, fifthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.5f;
  crossFade(currentColor, endStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.65f;
  crossFade(currentColor, fifthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.8f;
  crossFade(currentColor, endStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 1.2f;
  crossFade(currentColor, halfStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.7f;
  crossFade(currentColor, fifthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 1.35f;
  crossFade(currentColor, halfStep, fadeStep, currentDelay);


  currentDelay = fadeDelay * 0.15f;
  crossFade(currentColor, fourthStep, fadeStep, currentDelay);  //TODO: see the copypaste?
  currentDelay = fadeDelay * 0.35f;
  crossFade(currentColor, fifthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.5f;
  crossFade(currentColor, endStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.65f;
  crossFade(currentColor, fifthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.8f;
  crossFade(currentColor, endStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 1.2f;
  crossFade(currentColor, halfStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.7f;
  crossFade(currentColor, fifthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 1.35f;
  crossFade(currentColor, halfStep, fadeStep, currentDelay);

//  
//  currentDelay = fadeDelay * 0.85f;
//  crossFade(currentColor, brightest, fadeStep, currentDelay);
//  
//  currentDelay = fadeDelay * 0.65f;
//  crossFade(currentColor, halfStep, fadeStep, currentDelay);
//  
//  currentDelay = fadeDelay * 0.9f;
//  crossFade(currentColor, fourthStep, fadeStep, currentDelay);
//  
//  currentDelay = fadeDelay * 1.25f;
//  crossFade(currentColor, brightest, fadeStep, currentDelay);
//  
//  currentDelay = fadeDelay * 0.8f;
//  crossFade(currentColor, halfStep, fadeStep, currentDelay);
//
//  currentDelay = fadeDelay * 0.1f;
//  crossFade(currentColor, fourthStep, fadeStep, currentDelay);

  
  currentDelay = fadeDelay * 0.8f;
  crossFade(currentColor, halfStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.7f;
  crossFade(currentColor, fourthStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.8f;
  crossFade(currentColor, halfStep, fadeStep, currentDelay);
  currentDelay = fadeDelay * 0.7f;
  crossFade(currentColor, fourthStep, fadeStep, currentDelay);

  return true;

}




/////////////////////// For banjo


bool banjoSwitcher(){
  //const int numberOfStops = 6;
  int stops[6] = {10*4, 92*4, 134*4, 176*4, 218*4, 240*4};

  int delayDivider = 50;
  
  float fadeStep = 0.01;
  int programValue = analogRead(PROG_PIN);
  
      Serial.print(programValue);
  //HOLY CRAP THIS IS HORRID
  //if (programValue > stops[5]){
  //  Serial.println("program: viimeinen");
  //  return true;
  //}
    if (programValue > stops[4]){
      int staticDelay = 60;
      whiteStrobo(staticDelay);
      
    Serial.println("program: tokavika");
    return true;
  }
    if (programValue > stops[3]){
      policeColors(delayDivider);
      
    Serial.println("program: joku");
    return true;
  }
    if (programValue > stops[2]){
      //Rgbu randomColor = RandomColor(0);
      //crossFadePotentiometer(currentColor, randomColor, fadeStep, delayDivider);
      randomBanjo(fadeStep, delayDivider);
      //Serial.print("random is: ");
      //(PrintColors(randomColor));
    return true;
  }
    if (programValue > stops[1]){
      Serial.println("program: banjovalot");
      banjoLoop(delayDivider);
    return true;
  }
    if (programValue > stops[0]){
    Serial.println("program: valkoinen");
    crossFade(currentColor, white, fadeStep, potentiometerValue / delayDivider);
    return true;
  }
  
      Serial.println("program: valot pois");
  crossFade(currentColor, black, fadeStep, potentiometerValue / delayDivider);
  return true;
}

bool whiteStrobo(int staticDelay){
  setColor(white);
  delay(staticDelay);
  setColor(black);
  delay(staticDelay);
  
}

bool policeColors(int delayDivider){
  float fadeStep = 0.01;
  crossFadePotentiometer(currentColor, red, fadeStep, delayDivider);
  crossFadePotentiometer(currentColor, black, fadeStep, delayDivider);
  crossFadePotentiometer(currentColor, blue, fadeStep, delayDivider);
  crossFadePotentiometer(currentColor, black, fadeStep, delayDivider);
}

bool randomBanjo(float fadeStep, int delayDivider){

  int ran = random(9);

  switch(ran){
    case 0:
    crossFadePotentiometer(currentColor, red, fadeStep, delayDivider);
    break;
        case 1:
    crossFadePotentiometer(currentColor, blue, fadeStep, delayDivider);
    break;
    
        case 2:
    crossFadePotentiometer(currentColor, green, fadeStep, delayDivider);
    break;
    
        case 3:
    crossFadePotentiometer(currentColor, yellow, fadeStep, delayDivider);
    break;
    
        case 4:
    crossFadePotentiometer(currentColor, teal, fadeStep, delayDivider);
    break;
    
        case 5:
    crossFadePotentiometer(currentColor, violet, fadeStep, delayDivider);
    break;
    
        case 6:
    crossFadePotentiometer(currentColor, orange, fadeStep, delayDivider);
    break;
    
        case 7:
    crossFadePotentiometer(currentColor, pink, fadeStep, delayDivider);
    break;
    
        case 8:
    crossFadePotentiometer(currentColor, white, fadeStep, delayDivider);
    break;
  }
  
}

bool banjoLoop(int delayDivider){
  
  float fadeStep = 0.01;

  //first phase:
  
  //currentDelay = fadeDelay; // change between steps if needed
  crossFadePotentiometer(currentColor, red, fadeStep, delayDivider);
  crossFadePotentiometer(currentColor, green, fadeStep, delayDivider);
  crossFadePotentiometer(currentColor, blue, fadeStep, delayDivider);
    
}

